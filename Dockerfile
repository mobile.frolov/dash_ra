FROM python:3.8-slim-buster
LABEL maintainer "Frolov Viktor <frolov@taif.ru>"
WORKDIR /code
COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY ./ ./
EXPOSE 8050
CMD ["python", "./app.py"]
